package com.somospnt.reactive.sse.vo;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocumentVo {

    private String name;
    private LocalDateTime dateTime;

}
