package com.somospnt.reactive.sse.service;

import com.somospnt.reactive.sse.vo.DocumentVo;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class FileService {

    @Value("${reactive.sse.file.path}")
    private String path;

    public Flux<List<DocumentVo>> findAll() {
        return Flux.fromIterable(this.listFilesInPath()).collectList().repeat(1);
    }

    private List<DocumentVo> listFilesInPath() {
        List<DocumentVo> documents = null;
        try {
            documents = Files.list(Paths.get(path)).map(p -> new DocumentVo(p.getFileName().toString(), LocalDateTime.now())).collect(Collectors.toList());
        } catch (IOException ex) {
            Logger.getLogger(FileService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return documents;
    }

}
