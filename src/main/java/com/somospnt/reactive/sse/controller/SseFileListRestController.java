package com.somospnt.reactive.sse.controller;

import com.somospnt.reactive.sse.service.FileService;
import com.somospnt.reactive.sse.vo.DocumentVo;
import java.util.List;
import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SseFileListRestController {
    
    private final FileService fileService;
    
    public SseFileListRestController(FileService fileService) {
        this.fileService = fileService;
    }
    
    @GetMapping(value = "/api/files", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Publisher<List<DocumentVo>> findAll() {
        return this.fileService.findAll();
    }
    
}
