package com.somospnt.reactive.sse.controller;

import com.somospnt.reactive.sse.service.FileService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;

@Controller
public class SseFileListController {

    private final FileService fileService;

    public SseFileListController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/files")
    public String renderFileslView() {
        return "files";
    }

    @GetMapping(value = "/files-updates", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public String renderUpdatedFilesView(Model model) {
        ReactiveDataDriverContextVariable variable = new ReactiveDataDriverContextVariable(this.fileService.findAll(), 1);
        model.addAttribute("data", variable);
        return "files :: #files-block";
    }

}
